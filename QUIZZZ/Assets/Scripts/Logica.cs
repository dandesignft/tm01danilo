﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1. quem ficou com os 100 milhões de zeni como recompensa de paz?", 5);
        p1.AddResposta("Satan", false);
        p1.AddResposta("Goku", true);
        p1.AddResposta("Vegeta", false);
        p1.AddResposta("Bulma", false);

        ClassPergunta p2 = new ClassPergunta("2. por que majin boo é imune a magia de moro?", 3);
        p2.AddResposta("Por ser gordo.", false);
        p2.AddResposta("Por ser mais forte que moro.", false);
        p2.AddResposta("Por ser um ser magico.", true);
        p2.AddResposta("Nenhuma das respostas.", false);

        ClassPergunta p3 = new ClassPergunta("3. Na saga de goku black quantos zamasu aparecem?", 6);
        p3.AddResposta("Tres.", true);
        p3.AddResposta("Quatro.", false);
        p3.AddResposta("Cinco.", false);
        p3.AddResposta("Apenas um.", false);

        ClassPergunta p4 = new ClassPergunta("4. antes da fusão entre zamasu e goku black, na batalha quem era o mais forte presente ali?", 2);
        p4.AddResposta("Zamasu.", false);
        p4.AddResposta("Goku", false);
        p4.AddResposta("Vegeta.", true);
        p4.AddResposta("Goku black.", false);

        ClassPergunta p5 = new ClassPergunta("5. Quantos senhores zeno's existem na franquia ?", 5);
        p5.AddResposta("existem 4..", false);
        p5.AddResposta("exitem 2.", true);
        p5.AddResposta("existem 3.", false);
        p5.AddResposta("existem 1.", false);

        ClassPergunta p6 = new ClassPergunta("6.Quais personagens tem nome de comida?.", 5);
        p6.AddResposta("terraqueos.", false);
        p6.AddResposta("freeza.", false);
        p6.AddResposta("pikolo.", false);
        p6.AddResposta("os sayajin.", true);

        ClassPergunta p7 = new ClassPergunta("7. quem foi o oponente final em dragon ball super ate 2019?", 5);
        p7.AddResposta("freeza.", false);
        p7.AddResposta("broly.", false);
        p7.AddResposta("moro", true);
        p7.AddResposta("jiren.", false);

        ClassPergunta p8 = new ClassPergunta("8. quem venceu o torneio do poder?", 5);
        p8.AddResposta("goku", false);
        p8.AddResposta("'freeza'", false);
        p8.AddResposta("17", true);
        p8.AddResposta("kuririn", false);

        ClassPergunta p9 = new ClassPergunta("9. quem venceu o torneio do universo 6 e 7?", 5);
        p9.AddResposta("hitto", false);
        p9.AddResposta("gohan", false);
        p9.AddResposta("vegeta", false);
        p9.AddResposta("monaka", true);

        ClassPergunta p10 = new ClassPergunta("10. quem venceu hitto em dbs?", 5);
        p10.AddResposta("gohan", false);
        p10.AddResposta("goku", false);
        p10.AddResposta("vegeta", false);
        p10.AddResposta("monaka", true);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
